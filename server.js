const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const apiRoutes = require('./routes/api');
const path = require('path'); // Import the path module
const http = require('http'); // Import the http module for Socket.IO
const socketIo = require('socket.io'); // Import Socket.IO


const app = express();
const PORT = process.env.PORT || 4000;
const MONGODB_URI = 'mongodb://localhost:27017/mydatabase';

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

// Connect to MongoDB
mongoose.connect(MONGODB_URI)
    .then(() => console.log('Connected to MongoDB'))
    .catch(error => console.error('Error connecting to MongoDB:', error));

// Use API routes
app.use('/api', apiRoutes);


const server = http.createServer(app);

const io = socketIo(server);

// Handle WebSocket connections
io.on('connection', (socket) => {
    console.log('New client connected:', socket.id);

    // Handle form submission
    socket.on('submit-form', (formData) => {
        console.log('Form submitted by user:', formData);

        socket.join('live_users');
    });

    // Handle disconnection
    socket.on('disconnect', () => {
        console.log('Client disconnected:', socket.id);
    });
});

// Start the server
server.listen(PORT, () => {
    
    console.log(`Server is running on port ${PORT}`);
});
