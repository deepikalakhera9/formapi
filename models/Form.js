// models/Form.js
const mongoose = require('mongoose');

const formSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    mobile: String,
    email: String,
    loginId: { type: String, unique: true }, // Ensure uniqueness for loginId
    password: { type: String, unique: true }, 
    street: String,
    city: String,
    state: String,
    country: String
});

const FormModel = mongoose.model('Form', formSchema);

module.exports = FormModel;
