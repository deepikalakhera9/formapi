// routes/api.js
const express = require('express');
const router = express.Router();
const Item = require('../models/Item');

router.get('/items', async (req, res) => {
    try {
        const items = await Item.find();
        res.json(items);
    } catch (error) {
        console.error('Error fetching items:', error);
        res.status(500).json({ error: 'Failed to fetch items' });
    }
});

module.exports = router;
