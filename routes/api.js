const express = require('express');
const router = express.Router();
const FormModel = require('../models/Form');
const { io } = require('../app');

// Define the route for handling form submission
router.post('/submit-form', async (req, res) => {
    try {
        const formData = req.body;

        const existingUser = await FormModel.findOne({ loginId: formData.loginId });

        if (existingUser) {
            // If the username already exists, send a 400 status with an error message
            return res.status(400).json({ error: 'Username already in use' });
        }
      
        const savedData = await FormModel.create(formData); // Save data to MongoDB
        io.emit('join_live_users', savedData);
        res.status(201).json(savedData);

    } catch (error) {
        if (error.code === 11000) { // MongoDB duplicate key error
            res.status(400).json({ error: 'Username or password already in use' });
        } else {
            console.error('Error saving form data:', error);
            res.status(500).json({ error: 'Failed to save form data' });
        }
    }
});
module.exports = router;
// Define a route to retrieve all form data
router.get('/get-form-data', async (req, res) => {
    try {
        const formData = await FormModel.find({}); // Retrieve all form data from MongoDB
        res.status(200).json(formData);
    } catch (error) {
        console.error('Error retrieving form data:', error);
        res.status(500).json({ error: 'Failed to retrieve form data' });
    }
});


