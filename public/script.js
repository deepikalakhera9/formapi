$(document).ready(function() {
    $('#submitBtn').click(function(event) { // Listen for click event on submitBtn
        event.preventDefault();

        // Validate form fields
        if (!validateForm()) {
            return;
        }

        // If form is valid, submit the form
        $.ajax({
            type: 'POST',
            url: 'http://localhost:4000/api/submit-form', // Your API endpoint for handling form submission
            data: $('#myForm').serialize(),
            success: function(response) {
                alert('Form submitted successfully!');
                // Reset form fields
                $('#myForm')[0].reset();
            },
            error: function(error) {
                alert('Failed to submit form. Please try again.');
            }
        });
    });
});
const socket = io();
socket.on('join_live_users', (userData) => {
    console.log('User joined live_users room:', userData);
    // Join the "live_users" room
    socket.join('live_users');
});

function validateForm() {
    var mobileRegex = /^\d{10}$/;
    var emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    var loginIdRegex = /^[a-zA-Z0-9]{8,}$/;
    var passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;

    var firstName = $('#firstName').val().trim();
    var lastName = $('#lastName').val().trim();
    var mobile = $('#mobile').val().trim();
    var email = $('#email').val().trim();
    var loginId = $('#loginId').val().trim();
    var password = $('#password').val().trim();
    var street = $('#street').val().trim();
    var city = $('#city').val().trim();
    var state = $('#state').val().trim();
    var country = $('#country').val().trim();

    if (firstName === '' || lastName === '' || mobile === '' || email === '' || loginId === '' || password === '' || street === '' || city === '' || state === '' || country === '') {
        alert('All fields are required');
        return false;
    }

    if (!mobileRegex.test(mobile)) {
        alert('Mobile number must be 10 digits');
        return false;
    }

    if (!emailRegex.test(email)) {
        alert('Invalid email format');
        return false;
    }

    if (!loginIdRegex.test(loginId)) {
        alert('Login ID must be at least 8 characters long and alphanumeric');
        return false;
    }

    if (!passwordRegex.test(password)) {
        alert('Password must be at least 6 characters long, contain at least one uppercase letter, one lowercase letter, one digit, and one special character');
        return false;
    }

    return true; // Form is valid
}
